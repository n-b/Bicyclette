#!/bin/bash -e

pushd `dirname $0` > /dev/null
scriptpath=`pwd -P`
popd > /dev/null
projectroot=`dirname $scriptpath`

#
# 1. Run the Screenshots scheme
#   * in Release config
#   * in all languages
#   * on iPhone 4 and iPhone 5 screen sizes

languages=("fr" "en")
locales=("fr_FR" "en_GB" )

for device in "iPhone 4s" "iPhone 5" "iPhone 6" "iPhone 6 Plus" "iPad 2"; do
	echo starting $device
	for index in ${!languages[*]}; do
		echo starting ${languages[$index]}
		BIC_LANGUAGE=${languages[$index]} BIC_LOCALE=${locales[$index]}\
		DEBUG_INFORMATION_FORMAT=dwarf RUN_CLANG_STATIC_ANALYZER=NO\
		xcodebuild test -scheme Screenshots -configuration Release -destination "platform=iOS Simulator,name=$device"
	done
done

#
# 2. Composite the screenshots over the backgrounds for iTunes connect
#   (requires imagemagick and xmlstarlet : brew install imagemagick xmlstarlet)
#

screenshotsfolder=$projectroot/Screenshots
itcpackage=$screenshotsfolder/Bicyclette.itmsp
itcmetadata=$itcpackage/metadata.xml

for language in  "fr" "en"; do
	for file in `ls $screenshotsfolder/$language`; do
		if [[ "$file" == *Default* ]]; then
			continue;
		fi
		echo " "$file
		fgfile=$screenshotsfolder/$language/$file
		itcfilename=$(echo $language-$file | sed s/@/-/ )
		itcfile=$itcpackage/$itcfilename
		if [[ "$file" == *ipad* ]]; then
			barfile=$screenshotsfolder/status_bars/black-ipad.png
		elif [[ "$file" == *3x* ]]; then
			barfile=$screenshotsfolder/status_bars/black-iphone6plus.png
		elif [[ "$file" == *667* ]]; then
			barfile=$screenshotsfolder/status_bars/black-iphone6.png
		else
			barfile=$screenshotsfolder/status_bars/black-iphone.png
		fi
		bargeometry=+0+0

		composite -geometry $bargeometry $barfile $fgfile $itcfile &> /dev/null
		pngcrush -rem alla -rem text $itcfile $itcfile-crushed &> /dev/null
		mv $itcfile-crushed $itcfile

		xml ed -L -u //_:version//_:software_screenshot[_:file_name=\"$itcfilename\"]/_:checksum -v `md5 -q $itcfile` $itcmetadata
		xml ed -L -u //_:version//_:software_screenshot[_:file_name=\"$itcfilename\"]/_:size -v `stat -f%z $itcfile` $itcmetadata

	done
done

echo Screenshots Package Ready. Update version number and Use 
echo iTMSTransporter -m upload -f Screenshots/Bicyclette.itmsp -u nicolas.bouilleaud@gmail.com
echo to upload.