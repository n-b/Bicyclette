@import XCTest;
@import MapKit;

#import "CTT.h"
#import "XCTestCase+CTTScreenshot.h"
#import "MapVC.h"

@interface Screenshots : CATServerResponseTestCase
@end

@implementation NSString (NSRegularExpression)

- (NSRegularExpression*) bic_regexp
{
    return [NSRegularExpression regularExpressionWithPattern:self options:0 error:NULL];
}

@end

@implementation Screenshots

+ (void)setUp
{
    [super setUp];
    
    NSString * lang = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    if(![@[@"fr", @"en"] containsObject:lang]) {
        lang = @"fr";
    }
    // Save screenshots under Screenshots
    [[self class] ctt_setSnapshotsDirectory:[[@(getenv("PROJECT_DIR")) stringByAppendingPathComponent:@"screenshots"] stringByAppendingPathComponent:lang]];

    [[NSUserDefaults standardUserDefaults] setObject:@10 forKey:@"CTTStepTimeout"];
}

- (void)setUp
{
    [super setUp];
    id mapView = [[CTT match:@"BicycletteMapView"] view];
    MapVC * mapVC = (MapVC *)[CTT visibleViewController];
    mapVC.stickyTitleMode = YES;
    mapVC.navigationItem.prompt = nil;

    [[mapView valueForKey:@"attributionLabel"] setHidden:YES];
    [[mapVC valueForKey:@"scaleView"] setHidden:YES];
    
    self.responseStatusCode = 200;
    self.responseHeaders = @{};
}

- (void) moveToLat:(CLLocationDegrees)lat_ long:(CLLocationDegrees)long_ meters:(CLLocationDistance)meters_
{
    id mapView = [[CTT match:@"BicycletteMapView"] view];
    if([[UIDevice currentDevice] userInterfaceIdiom]==UIUserInterfaceIdiomPad) {
        meters_ += 1000;
    }
    [mapView setRegion:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(lat_,long_), meters_, meters_) animated:NO];
    [CTT waitUntil:^BOOL{
        MapVC * mapVC = (MapVC *)[CTT visibleViewController];
        return mapVC.rendering == YES;
    }];
    [CTT waitUntil:^BOOL{
        MapVC * mapVC = (MapVC *)[CTT visibleViewController];
        return mapVC.rendering == NO;
    }];
}

- (void) test00_Europe
{
    [self moveToLat:46.892686 long:3.520008 meters:2000000];
    [self ctt_saveSnapshot];
}

- (void) test01_Velib
{
    self.responseFilename = @"paris.json";

    [self moveToLat:48.857015 long:2.348206 meters:14500];
    [self ctt_saveSnapshot];
}

- (void) test02_Velib
{
    self.responseFilename = @"paris.json";

    [self moveToLat:48.84757730290756 long:2.366124461859616 meters:1250];
    [[CTT match:@"Gare D'austerlitz"] tap];
    id starButton = [CTT match:NSLocalizedString(@"station.favorite", nil)];
    if (![((id)[starButton view]) isSelected]) {
        [starButton tap];
    }
    [CTT match:NSLocalizedString(@"station.favorite", nil) options:CTTMatchOptionsSelected];
    [CTT pause:0.5];
    [self ctt_saveSnapshot];
}

- (void) test03_London
{
    self.responseFilename = @"london.xml";

    [self moveToLat:51.504445255 long:-0.160522468 meters:1700];
    
    [CTT tap:[@"Triangle Car Park" bic_regexp]];
    id starButton = [CTT match:NSLocalizedString(@"station.favorite", nil)];
    if (![((id)[starButton view]) isSelected]) {
        [starButton tap];
    }
    [CTT match:NSLocalizedString(@"station.favorite", nil) options:CTTMatchOptionsSelected];
    [[CTT frontview] tap];

    [self ctt_saveSnapshot];
}
- (void) test04_NYC
{
    self.responseFilename = @"newyork.json";
    [self moveToLat:40.73076228 long:-73.99000398 meters:7000];
    [self ctt_saveSnapshot];
}

- (void) testDefault
{
    MapVC * mapVC = (MapVC *)[CTT visibleViewController];
    UISegmentedControl * modeControl = [mapVC valueForKey:@"modeControl"];
    NSInteger oldSelectedSegmentIndex = modeControl.selectedSegmentIndex;
    NSString * oldTitle0 = [modeControl titleForSegmentAtIndex:0];
    NSString * oldTitle1 = [modeControl titleForSegmentAtIndex:1];
    modeControl.selectedSegmentIndex = UISegmentedControlNoSegment;
    [modeControl setTitle:@"" forSegmentAtIndex:0];
    [modeControl setTitle:@"" forSegmentAtIndex:1];

    id mapView = [[CTT match:@"BicycletteMapView"] view];
    [mapView setRegion:MKCoordinateRegionMakeWithDistance(CLLocationCoordinate2DMake(0,0), 0, 0) animated:NO];
    [CTT pause:0.1];
    [self ctt_saveSnapshot];
    
    modeControl.selectedSegmentIndex = oldSelectedSegmentIndex;
    [modeControl setTitle:oldTitle0 forSegmentAtIndex:0];
    [modeControl setTitle:oldTitle1 forSegmentAtIndex:1];
}

@end
