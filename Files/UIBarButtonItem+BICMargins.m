#import "UIBarButtonItem+BICMargins.h"

@implementation UIBarButtonItem (BICMargins)
+ (instancetype) bic_marginItemWithWidth:(CGFloat)width_
{
    UIBarButtonItem * item = [[self alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    item.width = width_;
    return item;
}
+ (instancetype) bic_flexibleMarginButtonItem
{
    return [[self alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
}
@end
