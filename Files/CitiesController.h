@import Foundation;
@import MapKit;

@class BicycletteCity;
@class Station;
@class GeofencesMonitor;

@protocol CitiesControllerDelegate;

@interface CitiesController : NSObject
@property (readonly) GeofencesMonitor * fencesMonitor;
@property NSArray * cities;
- (BicycletteCity*) cityNamed:(NSString*)cityName;
@property (readonly, nonatomic) BicycletteCity * currentCity;
@property (assign) id<CitiesControllerDelegate> delegate;
@property (nonatomic) BOOL mapViewIsMoving;
- (void) regionDidChange:(MKCoordinateRegion)region;
- (void) handleLocalNotificaion:(UILocalNotification*)notification;
- (void) selectStationNumber:(NSString*)stationNumber inCityNamed:(NSString*)cityName changeRegion:(BOOL)changeRegion;
- (void) selectCity:(BicycletteCity*)city_;
- (void) switchStarredStation:(Station*)station;
@end


@protocol CitiesControllerDelegate <NSObject>
- (void) controller:(CitiesController*)controller setRegion:(MKCoordinateRegion)region;
- (void) controller:(CitiesController*)controller setAnnotations:(NSArray*)newAnnotations overlays:(NSArray*)newOverlays dataUpdated:(BOOL)dataUpdated;
- (void) controller:(CitiesController*)controller selectAnnotation:(id<MKAnnotation>)annotation;
@end
