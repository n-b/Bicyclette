#import "CityOverlayRenderer.h"
#import "BicycletteCity.h"
#import "Station.h"
#import "Station+Update.h"
#import "Style.h"
#import "BicycletteCity+Update.h"

@interface CityOverlayRenderer ()
@property (readonly) BicycletteCity * city;
@end

@implementation CityOverlayRenderer

- (BicycletteCity *)city
{
    return (BicycletteCity *)self.overlay;
}

- (void)drawMapRect:(MKMapRect)mapRect
          zoomScale:(MKZoomScale)zoomScale
          inContext:(CGContextRef)context
{

    // stations will be drawn using a rect whose size depends on the scale
    CGFloat stationRadius = MKRoadWidthAtZoomScale(zoomScale)*1.5;
    
    // We're going to need a slightly larger rect to draw stations that are just near the border
    CGRect rect = CGRectInset([self rectForMapRect:mapRect], -stationRadius, -stationRadius);
    mapRect = [self mapRectForRect:rect];
    
    // Fetch all the stations in this rect
    NSArray * stations = [self.city cachedStationsStatus];
    for (NSDictionary * station in stations) {
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([station[Station.attributes.latitude] doubleValue], [station[Station.attributes.longitude] doubleValue]);
        MKMapPoint mapPoint = MKMapPointForCoordinate(coordinate);
        if(!MKMapRectContainsPoint(mapRect, mapPoint)) {
            continue;
        }
        CGPoint point = [self pointForMapPoint:mapPoint];
        
        UIColor * color;
        
        if([station[Station.attributes.status_date] bic_isFresh] && [station[Station.attributes.open] boolValue]) {
            int16_t value;
            if(self.mode==StationAnnotationModeBikes)
                value = [station[Station.attributes.status_available] intValue];
            else
                value = [station[Station.attributes.status_free] intValue];
            
            if(value==0) color = kCriticalValueColor;
            else if(value<4) color = kWarningValueColor;
            else color = kGoodValueColor;
        } else {
            color = kUnknownValueColor;
        }
        CGFloat alpha = ([station[Station.attributes.starred] boolValue] || self.onlyDisplayFavorites==NO)? .8 : .2;
        color = [color colorWithAlphaComponent:alpha];
        CGContextSetFillColorWithColor(context, color.CGColor);

        CGRect stationRect = CGRectMake(point.x-stationRadius, point.y-stationRadius, stationRadius * 2, stationRadius * 2);
        if(self.mode==StationAnnotationModeBikes) {
            CGContextFillEllipseInRect(context, CGRectIntegral(stationRect));
        } else {
            CGPathRef path = CGPathCreateWithRoundedRect(stationRect, stationRadius/2, stationRadius/2, NULL);
            CGContextAddPath(context, path);
            CGPathRelease(path);
            CGContextFillPath(context);
        }
    }
}

@end
