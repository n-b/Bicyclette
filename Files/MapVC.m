#import "MapVC.h"
#import "BicycletteCity+Update.h"
#import "Station+Update.h"
#import "CollectionsAdditions.h"
#import "StationAnnotationView.h"
#import "MapViewScaleView.h"
#import "GeofencesMonitor.h"
#import "Style.h"
#import "CityAnnotationView.h"
#import "MKUtilities.h"
#import "CityOverlayRenderer.h"
#import "PrefsVC.h"
#import "UIBarButtonItem+BICMargins.h"

@interface MapVC() <CLLocationManagerDelegate>
// UI
@property MKMapView * mapView;
@property MKUserTrackingBarButtonItem * userTrackingButton;
@property CLLocationManager * locationManager;
@property UISegmentedControl * modeControl;
@property UIBarButtonItem * favoritesButton;
@property UIBarButtonItem * infoButton;
@property MapViewScaleView * scaleView;

@property StationAnnotationMode stationMode;
@property BOOL onlyDisplayFavorites;
@property BOOL shouldZoomToUserWhenLocationFound;
@property BOOL rendering;

@property UIView * statusBarBackground;
@end


/****************************************************************************/
#pragma mark -

@implementation MapVC

+ (instancetype) mapVCWithController:(CitiesController*)controller_
{
    MapVC * mapVC = [self new];
    mapVC.controller = controller_;
    return mapVC;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cityDataUpdated:) name:BicycletteCityNotifications.updateBegan object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cityDataUpdated:) name:BicycletteCityNotifications.updateGotNewData object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cityDataUpdated:) name:BicycletteCityNotifications.updateSucceeded object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cityDataUpdated:) name:BicycletteCityNotifications.updateFailed object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActive) name:UIApplicationDidBecomeActiveNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActive) name:UIApplicationWillResignActiveNotification object:nil];
        
        self.locationManager = [CLLocationManager new];
        self.locationManager.delegate = self;
    }
    return self;
}

- (void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) setController:(CitiesController *)controller_
{
    [_controller removeObserver:self forKeyPath:@"currentCity" context:__FILE__];
    _controller = controller_;
    [_controller addObserver:self forKeyPath:@"currentCity" options:0 context:__FILE__];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == __FILE__ && object==self.controller && [keyPath isEqualToString:@"currentCity"]) {
        [self updateModeControl];
        [self updateTitle];
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

/****************************************************************************/
#pragma mark View Cycle

- (void) loadView
{
    [super loadView]; // get a base view

    // Compute frames
    // on iPhone, the toolbar is transparent and the mapview is visible beneath it.
    // on iPad, it's opaque.
#define kToolbarHeight 44 // Strange. I was expecting to find a declared constant for it.
    CGRect mapViewFrame, toolBarFrame, frameAboveToolbar;
    CGRectDivide(self.view.bounds, &toolBarFrame, &frameAboveToolbar, kToolbarHeight, CGRectMaxYEdge);
    mapViewFrame = self.view.bounds;

    // Create mapview
    self.mapView = [[MKMapView alloc]initWithFrame:mapViewFrame];
    [self.view addSubview:self.mapView];
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.mapView.zoomEnabled = YES;
    self.mapView.showsPointsOfInterest = NO;
    self.mapView.showsBuildings = NO;
    self.mapView.scrollEnabled = YES;
    self.mapView.delegate = self;
    self.mapView.accessibilityIdentifier = @"BicycletteMapView";
    
    [self restoreMapRegion];
    
    // prepare toolbar items
    self.userTrackingButton = [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.mapView];
    
    self.modeControl = [[UISegmentedControl alloc] initWithItems:@[ NSLocalizedString(@"mode.bike", nil), NSLocalizedString(@"mode.docks", nil) ]];
    [self.modeControl addTarget:self action:@selector(switchMode:) forControlEvents:UIControlEventValueChanged];
    self.modeControl.selectedSegmentIndex = self.stationMode;
    UIBarButtonItem * modeItem = [[UIBarButtonItem alloc] initWithCustomView:self.modeControl];
    modeItem.width = 140;
    
    self.favoritesButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Star-off"] style:UIBarButtonItemStyleBordered target:self action:@selector(switchFavorites:)];
    self.favoritesButton.width = self.userTrackingButton.width;

    UIButton * iButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [iButton addTarget:self action:@selector(showPrefsVC) forControlEvents:UIControlEventTouchUpInside];
    self.infoButton = [[UIBarButtonItem alloc] initWithCustomView:iButton];
    
    // create toolbar
    self.toolbarItems = @[[UIBarButtonItem bic_marginItemWithWidth:-8],
                          self.userTrackingButton,
                          self.favoritesButton,
                          [UIBarButtonItem bic_flexibleMarginButtonItem],
                          modeItem,
                          [UIBarButtonItem bic_flexibleMarginButtonItem],
                          [UIBarButtonItem bic_marginItemWithWidth:self.favoritesButton.width+8],
                          [UIBarButtonItem bic_flexibleMarginButtonItem],
                          self.infoButton,
                          [UIBarButtonItem bic_marginItemWithWidth:-8]];
    
    self.navigationController.toolbarHidden = NO;
    self.navigationController.toolbar.translucent = YES;
    
    // Add Scale
    CGRect scaleFrame = frameAboveToolbar;
    
    CGRect nothing;
    CGFloat scaleViewMargin = 9;
    CGFloat scaleViewWidth = 100;
    CGFloat scaleViewHeight = 13;
    CGRectEdge xEdge, yEdge;
    UIViewAutoresizing resizingMask;
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        xEdge = CGRectMinXEdge;
        yEdge = CGRectMinYEdge;
        resizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        CGRectDivide(scaleFrame, &nothing, &scaleFrame, [[UIScreen mainScreen] applicationFrame].origin.y, CGRectMinYEdge);
    }
    else
    {
        xEdge = CGRectMaxXEdge;
        yEdge = CGRectMaxYEdge;
        resizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleTopMargin;
    }

    // Margins
    CGRectDivide(scaleFrame, &nothing, &scaleFrame, scaleViewMargin, xEdge);
    CGRectDivide(scaleFrame, &nothing, &scaleFrame, scaleViewMargin, yEdge);
    // Contents
    CGRectDivide(scaleFrame, &scaleFrame, &nothing, scaleViewWidth, xEdge);
    CGRectDivide(scaleFrame, &scaleFrame, &nothing, scaleViewHeight, yEdge);

    self.scaleView = [[MapViewScaleView alloc] initWithFrame:scaleFrame];
    self.scaleView.autoresizingMask = resizingMask;
    [self.view addSubview:self.scaleView];
    self.scaleView.mapView = self.mapView;
    
    // Navigation bar
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    [self updateTitle];

    if([UIVisualEffectView class]) {
        CGRect statusBarFrame = [UIApplication sharedApplication].statusBarFrame;
        self.statusBarBackground = [[UIView alloc] initWithFrame:statusBarFrame];
        [self.view addSubview:self.statusBarBackground];

        UIVisualEffectView * blur = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight]];
        [self.statusBarBackground addSubview:blur];
        UIView * line = [UIView new]; line.backgroundColor = [UIColor.grayColor colorWithAlphaComponent:.5];
        [self.statusBarBackground addSubview:line];

        CGRect lineFrame, blurFrame;
        CGFloat onePixel = 1/[UIScreen mainScreen].scale;
        CGRectDivide(statusBarFrame, &lineFrame, &blurFrame, onePixel, CGRectMaxYEdge);
        line.frame = lineFrame;
        blur.frame = blurFrame;
        line.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        blur.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.shouldZoomToUserWhenLocationFound = YES;
}

- (void) appDidBecomeActive
{
    self.shouldZoomToUserWhenLocationFound = YES;
}

- (void) appWillResignActive
{
    [self saveMapRegion];
}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.statusBarBackground.frame = [UIApplication sharedApplication].statusBarFrame;
}

- (void) saveMapRegion
{
    MKCoordinateRegion region = self.mapView.region;
    NSDictionary * r = @{@"center":@{ @"latitude": @(region.center.latitude),
                                      @"longitude": @(region.center.longitude), },
                         @"span":@{ @"latitudeDelta": @(region.span.latitudeDelta),
                                    @"longitudeDelta": @(region.span.longitudeDelta) }, };
    [[NSUserDefaults standardUserDefaults] setObject:r forKey:@"MapVC.region"];
}

- (void) restoreMapRegion
{
    NSDictionary * r = [[NSUserDefaults standardUserDefaults] objectForKey:@"MapVC.region"];
    if(r) {
        MKCoordinateRegion region = MKCoordinateRegionMake(CLLocationCoordinate2DMake([r[@"center"][@"latitude"] doubleValue],
                                                                                      [r[@"center"][@"longitude"] doubleValue]),
                                                           MKCoordinateSpanMake([r[@"span"][@"latitudeDelta"] doubleValue],
                                                                                [r[@"span"][@"longitudeDelta"] doubleValue]));
        self.mapView.region = region;
    }
}

- (void) updateTitle
{
    if(self.controller.currentCity) {
        [self showTitle:self.controller.currentCity.title subtitle:nil sticky:NO];
    } else {
        [self dismissTitle];
    }
}

- (void) cityDataUpdated:(NSNotification*)note
{
    if(note.object==self.controller.currentCity) {
        if([note.name isEqualToString:BicycletteCityNotifications.updateBegan]) {
            [self showTitle:self.controller.currentCity.title
                   subtitle:[NSString stringWithFormat:NSLocalizedString(@"update.status.fetching", nil)]
                     sticky:YES];
        } else if([note.name isEqualToString:BicycletteCityNotifications.updateGotNewData]) {
            [self showTitle:self.controller.currentCity.title
                   subtitle:[NSString stringWithFormat:NSLocalizedString(@"update.status.parsing", nil)]
                     sticky:YES];
        } else if([note.name isEqualToString:BicycletteCityNotifications.updateSucceeded]){
            [self showTitle:self.controller.currentCity.title
                   subtitle:[NSString stringWithFormat:NSLocalizedString(@"update.status.completed", nil)]
                     sticky:NO];
        } else if([note.name isEqualToString:BicycletteCityNotifications.updateFailed]) {
            [self showTitle:self.controller.currentCity.title
                   subtitle:[NSString stringWithFormat:NSLocalizedString(@"update.status.failed", nil)]
                     sticky:NO];
        }
    }
}

- (void) updateModeControl
{
    if(self.controller.currentCity==nil || [self.controller.currentCity canShowFreeSlots]) {
        self.modeControl.hidden = NO;
    } else {
        self.modeControl.hidden = YES;
        self.stationMode = StationAnnotationModeBikes;
    }
}

/****************************************************************************/
#pragma mark Rotation Support

- (BOOL) shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

/****************************************************************************/
#pragma mark CitiesControllerDelegate

- (void) controller:(CitiesController*)controller setRegion:(MKCoordinateRegion)region
{
    [self.mapView setRegion:region animated:YES];
}

- (void) controller:(CitiesController*)controller selectAnnotation:(id<MKAnnotation>)annotation_
{
    if(annotation_)
        [self.mapView selectAnnotation:annotation_ animated:YES];
    else
    {
        for (id<MKAnnotation>annotation in [self.mapView selectedAnnotations]) {
            [self.mapView deselectAnnotation:annotation animated:YES];
        }
    }
}

- (void) controller:(CitiesController*)controller setAnnotations:(NSArray*)newAnnotations_ overlays:(NSArray*)newOverlays_ dataUpdated:(BOOL)dataUpdated_
{
    NSArray * oldAnnotations = [self.mapView.annotations arrayByRemovingObjectsInArray:@[ self.mapView.userLocation ]];
    [self.mapView removeAnnotations:[oldAnnotations arrayByRemovingObjectsInArray:newAnnotations_]];
    [self.mapView addAnnotations:[newAnnotations_ arrayByRemovingObjectsInArray:oldAnnotations]];
    
    NSArray * oldOverlays = self.mapView.overlays;
    BOOL overlaysChanged = ![oldOverlays isEqualToArray:newOverlays_];
    if(overlaysChanged) {
        [self.mapView removeOverlays:[oldOverlays arrayByRemovingObjectsInArray:newOverlays_]];
        [self.mapView addOverlays:[newOverlays_ arrayByRemovingObjectsInArray:oldOverlays]];
    }
    if(dataUpdated_){
        [[self.mapView rendererForOverlay:self.controller.currentCity] setNeedsDisplay];
    }
}

/****************************************************************************/
#pragma mark MapView Delegate

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    // "animated" means "not dragged by the user"
    // I only want to know that the mapview is being manipulated by the user.
    if(!animated) {
        self.controller.mapViewIsMoving = YES;
    }
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    self.controller.mapViewIsMoving = NO;
    [self.controller regionDidChange:self.mapView.region];
    [self.scaleView setNeedsDisplay];
    self.shouldZoomToUserWhenLocationFound = NO;
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView_ viewForAnnotation:(id <MKAnnotation>)annotation
{
	if(annotation == self.mapView.userLocation)
		return nil;
	else if([annotation isKindOfClass:[Station class]])
	{
		StationAnnotationView * stationAV = (StationAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"Station"];
		if(nil==stationAV) {
			stationAV = [[StationAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"Station"];
        }
        
        stationAV.mode = self.stationMode;
        stationAV.onlyDisplayFavorites = self.onlyDisplayFavorites;
		return stationAV;
	}
	else if([annotation isKindOfClass:[BicycletteCity class]])
	{
        CityAnnotationView * pinAV = (CityAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"pin"];
        if(nil==pinAV) {
            pinAV = [[CityAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"pin"];
        } else {
            pinAV.annotation = annotation;
        }
        return pinAV;
	}
	return nil;
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if([overlay isKindOfClass:[Geofence class]]) {
        MKCircleRenderer * circleRenderer = [[MKCircleRenderer alloc] initWithOverlay:overlay];
        circleRenderer.fillColor = kFenceBackgroundColor;
        return circleRenderer;
    } else if ([overlay isKindOfClass:[BicycletteCity class]]) {
        CityOverlayRenderer * cityRenderer = [[CityOverlayRenderer alloc] initWithOverlay:overlay];
        cityRenderer.mode = self.stationMode;
        cityRenderer.onlyDisplayFavorites = self.onlyDisplayFavorites;
        return cityRenderer;
    } else {
        return nil;
    }
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    if( self.shouldZoomToUserWhenLocationFound )
    {
        // This is the first location update we get : if the user hasn't moved the map, and he is physically inside a city, let's zoom.
        BicycletteCity * nearestCity = (BicycletteCity*)[self.controller.cities nearestLocatableFrom:userLocation.location];
        if([[nearestCity regionContainingData] containsCoordinate:userLocation.location.coordinate])
        {
            [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
        }
    }
    self.shouldZoomToUserWhenLocationFound = NO;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
	if([view.annotation isKindOfClass:[BicycletteCity class]]) {
        [mapView deselectAnnotation:view.annotation animated:NO];
        // Are there other cities near the tapped annotation?
        // If some annotations are very close to each other,
        // We want to zoom in gradually and let the user pick the right one
        // instead of directly opening the tapped annotation.
        CGRect nearBox = CGRectStandardize(CGRectInset([view frame],-10,-10)); // A rect around the annotation view
        MKCoordinateRegion nearRegion = [mapView convertRect:nearBox toRegionFromView:[view superview]];
        MKMapRect nearRect = BICMKMapRectForCoordinateRegion(nearRegion);
        // Ask the mapview for other annotations in the near rect.
        NSSet * nearCities = [mapView annotationsInMapRect:nearRect];
        nearCities = [nearCities filteredSetUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id obj, NSDictionary *bindings) {
            return [obj isKindOfClass:[BicycletteCity class]];
        }]];
        if([nearCities count]>1) {
            MKCoordinateRegion zoomRegion = [[nearCities allObjects] locatable_region];
            zoomRegion.span.latitudeDelta *= 2; // Leave some space around
            zoomRegion.span.longitudeDelta *= 2;
            [self.mapView setRegion:zoomRegion animated:YES];
        } else {
            // Otherwise select it!
            [self.controller selectCity:(BicycletteCity*)view.annotation];
        }
    } else if([view.annotation isKindOfClass:[Station class]]) {
        if([self.controller.currentCity canUpdateIndividualStations])
            [(Station*)view.annotation updateWithCompletionBlock:nil];
    }
}


- (void)mapViewWillStartRenderingMap:(MKMapView *)mapView NS_AVAILABLE(10_9, 7_0)
{
    self.rendering = YES;
}

- (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView fullyRendered:(BOOL)fullyRendered
{
    if(fullyRendered) {
        self.rendering = NO;
    }
}

- (void)mapView:(MKMapView *)mapView annotationView:(StationAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    if([view isKindOfClass:[StationAnnotationView class]]) {
        NSAssert([view.annotation isKindOfClass:[Station class]],nil);
        Station * station = (Station*)view.annotation;
        [self.controller switchStarredStation:station];
    }
}

/****************************************************************************/
#pragma mark User Location and authorization

- (void)mapViewWillStartLoadingMap:(MKMapView *)mapView
{
    [self updateUserTrackingMode];
}

- (void) mapViewWillStartLocatingUser:(MKMapView *)mapView
{
    [self updateUserTrackingMode];
}

- (void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    [self updateUserTrackingMode];
}

- (void) updateUserTrackingMode
{
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusNotDetermined:
            if(self.mapView.userTrackingMode != MKUserTrackingModeNone && [self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [self.locationManager requestWhenInUseAuthorization];
            }
            break;
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            self.mapView.userTrackingMode = MKUserTrackingModeNone;
            self.userTrackingButton.enabled = NO;
            break;
        }
        case kCLAuthorizationStatusAuthorizedAlways:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        {
            self.userTrackingButton.enabled = YES;
            self.mapView.showsUserLocation = YES;
        }
        default:
            break;
    }
}

/****************************************************************************/
#pragma mark Actions

- (void) switchMode:(UISegmentedControl*)sender
{
    self.stationMode = sender.selectedSegmentIndex;
    for (id<MKAnnotation> annotation in [self.mapView.annotations filteredArrayWithValue:[Station class] forKeyPath:@"class"]) {
        StationAnnotationView * stationAV = (StationAnnotationView*)[self.mapView viewForAnnotation:annotation];
        stationAV.mode = self.stationMode;
    }
    CityOverlayRenderer * cityRenderer = (CityOverlayRenderer *)[self.mapView rendererForOverlay:self.controller.currentCity];
    cityRenderer.mode = self.stationMode;
    [cityRenderer setNeedsDisplay];
}

- (void) switchFavorites:(UISegmentedControl*)sender
{
    self.onlyDisplayFavorites = !self.onlyDisplayFavorites;
    self.favoritesButton.image = self.onlyDisplayFavorites ? [UIImage imageNamed:@"Star-on"] : [UIImage imageNamed:@"Star-off"];
    [self.favoritesButton setValue:@(self.onlyDisplayFavorites) forKey:@"selected"]; // private api !
    for (id<MKAnnotation> annotation in [self.mapView.annotations filteredArrayWithValue:[Station class] forKeyPath:@"class"]) {
        StationAnnotationView * stationAV = (StationAnnotationView*)[self.mapView viewForAnnotation:annotation];
        stationAV.onlyDisplayFavorites = self.onlyDisplayFavorites;
    }
    CityOverlayRenderer * cityRenderer = (CityOverlayRenderer *)[self.mapView rendererForOverlay:self.controller.currentCity];
    cityRenderer.onlyDisplayFavorites = self.onlyDisplayFavorites;
    [cityRenderer setNeedsDisplay];
    if(self.onlyDisplayFavorites) {
        NSArray * starredStations = self.controller.currentCity.starredStations;
        if ([starredStations count]) {
            MKCoordinateRegion starredRegion = [starredStations locatable_region];
            starredRegion.span.latitudeDelta *= 2; // Leave some space around
            starredRegion.span.longitudeDelta *= 2;
            [self.mapView setRegion:starredRegion animated:YES];
        }
    }
}

- (void) showPrefsVC
{
    [self presentViewController:[PrefsVC prefsVCWithController:self.controller] animated:YES completion:nil];
}

// Banner

- (void) showTitle:(NSString*)title subtitle:(NSString*)subtitle sticky:(BOOL)sticky
{
    self.navigationItem.title = title;
    if(!self.stickyTitleMode) {
        self.navigationItem.prompt = subtitle;
    }
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismissTitle) object:nil];
    
    if(!sticky && !self.stickyTitleMode) {
        [self performSelector:@selector(dismissTitle) withObject:nil afterDelay:3];
    }
}

- (void) dismissTitle
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:_cmd object:nil];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

@end
