@import UIKit;

@class CitiesController;

@interface PrefsVC : UITableViewController
// Create a prefsVC embedded in a navigation controller
+ (UIViewController*) prefsVCWithController:(CitiesController *)controller;
@end
