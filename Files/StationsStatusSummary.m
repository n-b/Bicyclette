#import "StationsStatusSummary.h"
#import "BicycletteCity.h"
#import "BicycletteCity+Update.h"
#import "GeofencesMonitor.h"
#import "CollectionsAdditions.h"

@implementation UIApplication (LocalAlerts)

- (void) presentNotificationWithSummary:(NSString*)summary_ forStation:(Station*)station_ withSound:(BOOL)sound_
{
    UILocalNotification * notification = [UILocalNotification new];
    notification.alertBody = summary_;
    notification.hasAction = YES;
    notification.soundName = sound_ ? @"bell.wav" : nil;
    notification.userInfo = @{@"type": @"stationsummary",
                              @"city": station_.city.cityName ,
                              @"stationNumber": station_.number};
    [self presentLocalNotificationNow:notification];
}

@end

#pragma mark - StationsStatusSummary

@implementation StationsStatusSummary
{
    UILocalNotification * _progressIndication;
    NSArray * _fences;
    Geofence * _referenceFence;
    BicycletteCity * _city;
    BOOL _entered;
}

- (id) initWithFences:(NSArray*)fences_ relativeToFence:(Geofence*)fence_ entered:(BOOL)entered_
{
    // Prevent displaying summaries to frequently
    NSDate * lastSummaryDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"StationsStatusSummary.lastSummaryDate"];
    NSDate * now = [NSDate date];
    NSTimeInterval minTimeInterval = [[NSUserDefaults standardUserDefaults] doubleForKey:@"StationsStatusSummary.minimumTimeInterval"];
    NSTimeInterval retryTimeInterval = [[NSUserDefaults standardUserDefaults] doubleForKey:@"StationsStatusSummary.retryTimeInterval"];
    // Don't run a summary if the latest was ran recently,
    // But run a new summary anyway if the latest was just seconds ago, to accomodate GeofenceMonitor glitches
    if(lastSummaryDate && ([now timeIntervalSinceDate:lastSummaryDate]<minTimeInterval && [now timeIntervalSinceDate:lastSummaryDate]>retryTimeInterval)) {
        return nil;
    }
    
    
    self = [super init];
    if(self!=nil){
        NSParameterAssert(fences_);
        NSParameterAssert(fence_);
        NSAssert([fences_ containsObject:fence_], nil);
        _fences = fences_;
        _referenceFence = fence_;
        _city = _referenceFence.city;
        _entered = entered_;

        _progressIndication = [UILocalNotification new];
        _progressIndication.alertBody = NSLocalizedString(@"update.status.parsing", nil);
        _progressIndication.hasAction = NO;
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        [[UIApplication sharedApplication] presentLocalNotificationNow:_progressIndication];
    }
    return self;
}

- (NSOrderedSet*) stations
{
    NSMutableOrderedSet * stations = [NSMutableOrderedSet new];
    for (Geofence * fence in _fences) {
        [stations addObjectsFromArray:[fence stations]];
    }
    if(!_entered) {
        [stations removeObjectsInArray:[_referenceFence stations]];
    }
    return stations;
}

- (CLLocation *)location
{
    return [_city location];
}

- (void) cancel
{
    if(_progressIndication) {
        [[UIApplication sharedApplication] cancelLocalNotification:_progressIndication];
    }
    _progressIndication = nil;
}

- (void) presentSummary
{
    if(_progressIndication) {
        [[UIApplication sharedApplication] cancelLocalNotification:_progressIndication];
        _progressIndication = nil;
    }
    
    NSInteger warningValue = [[NSUserDefaults standardUserDefaults] integerForKey:@"StationStatus.warningValue"];
    // Display available bikes in all other fences
    NSMutableArray * fences = [_fences mutableCopy];
    [fences removeObject:_referenceFence];
    for (Geofence * remoteFence in fences) {
        BOOL shouldPlaySound = NO;
        NSArray * stationsWithEnoughSlots = [remoteFence.stations filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K > %d",Station.attributes.status_free, warningValue]];
        NSArray * stationsWithFewSlots = [remoteFence.stations filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K BETWEEN { 1 , %d }",Station.attributes.status_free, warningValue]];
        NSArray * stationsWithNoSlots = [remoteFence.stations filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == 0",Station.attributes.status_free]];
        
        NSMutableString * message = [NSMutableString new];
        if([stationsWithEnoughSlots count]>0) {
            [message appendFormat:@"%@ %@",NSLocalizedString(@"notifications.parking.OK",nil),NSLocalizedString(@"notifications.parking.available.at", nil)];
            NSMutableArray * stationsStatus = [NSMutableArray new];
            for (Station * station in stationsWithEnoughSlots) {
                [stationsStatus addObject:[NSString stringWithFormat:NSLocalizedString(@"notifications.station.%@.status.%d.total.%d", nil),
                                           station.title,
                                           station.status_free,
                                           station.status_free+station.status_available]];
            }
            [message appendString:[stationsStatus componentsJoinedByString:@", "]];
            [message appendString:@"."];
        }
        if([stationsWithFewSlots count]>0) {
            if([message length]) {
                [message appendString:@" "];
            }
            [message appendFormat:@"%@ %@",NSLocalizedString(@"notifications.parking.warning",nil),NSLocalizedString(@"notifications.parking.fewavailable.at", nil)];
            NSMutableArray * stationsStatus = [NSMutableArray new];
            for (Station * station in stationsWithFewSlots) {
                [stationsStatus addObject:[NSString stringWithFormat:NSLocalizedString(@"notifications.station.%@.status.%d.total.%d", nil),
                                           station.title,
                                           station.status_free,
                                           station.status_free+station.status_available]];
            }
            [message appendString:[stationsStatus componentsJoinedByString:@", "]];
            [message appendString:@"."];
        }
        if([stationsWithNoSlots count]>0) {
            if([message length]) {
                [message appendString:@" "];
            }
            [message appendFormat:@"%@ %@",NSLocalizedString(@"notifications.parking.KO",nil),NSLocalizedString(@"notifications.parking.notavailable.at", nil)];
            shouldPlaySound = YES;
            NSMutableArray * stationsStatus = [NSMutableArray new];
            for (Station * station in stationsWithNoSlots) {
                [stationsStatus addObject:station.title];
            }
            [message appendString:[stationsStatus componentsJoinedByString:@", "]];
            [message appendString:@"."];
        }
        [[UIApplication sharedApplication] presentNotificationWithSummary:message
                                                               forStation:[remoteFence.stations firstObject]
                                                                withSound:shouldPlaySound];
    }
    
    // Present notification for referenceFence *last* so it appears *above* the others.
    if(_entered) {
        BOOL shouldPlaySound = NO;
        NSArray * stationsWithEnoughBikes = [_referenceFence.stations filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K > %d",Station.attributes.status_available, warningValue]];
        NSArray * stationsWithFewBikes = [_referenceFence.stations filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K BETWEEN { 1 , %d }",Station.attributes.status_available, warningValue]];
        NSArray * stationsWithNoBikes = [_referenceFence.stations filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%K == 0",Station.attributes.status_available]];

        NSMutableString * message = [NSMutableString new];
        if([stationsWithEnoughBikes count]>0) {
            [message appendFormat:@"%@ %@",NSLocalizedString(@"notifications.bikes.OK",nil),NSLocalizedString(@"notifications.bikes.available.at", nil)];
            NSMutableArray * stationsStatus = [NSMutableArray new];
            for (Station * station in stationsWithEnoughBikes) {
                [stationsStatus addObject:[NSString stringWithFormat:NSLocalizedString(@"notifications.station.%@.status.%d.total.%d", nil),
                                           station.title,
                                           station.status_available,
                                           station.status_free+station.status_available]];
            }
            [message appendString:[stationsStatus componentsJoinedByString:@", "]];
            [message appendString:@"."];
        }
        if([stationsWithFewBikes count]>0) {
            if([message length]) {
                [message appendString:@" "];
            }
            [message appendFormat:@"%@ %@",NSLocalizedString(@"notifications.bikes.warning",nil),NSLocalizedString(@"notifications.bikes.fewavailable.at", nil)];
            NSMutableArray * stationsStatus = [NSMutableArray new];
            for (Station * station in stationsWithFewBikes) {
                [stationsStatus addObject:[NSString stringWithFormat:NSLocalizedString(@"notifications.station.%@.status.%d.total.%d", nil),
                                           station.title,
                                           station.status_available,
                                           station.status_free+station.status_available]];
            }
            [message appendString:[stationsStatus componentsJoinedByString:@", "]];
            [message appendString:@"."];
        }
        // insert stations with few bikes
        if([stationsWithNoBikes count]>0) {
            if([message length]) {
                [message appendString:@" "];
            }
            [message appendFormat:@"%@ %@",NSLocalizedString(@"notifications.bikes.KO",nil),NSLocalizedString(@"notifications.bikes.notavailable.at", nil)];
            shouldPlaySound = YES;
            NSMutableArray * stationsStatus = [NSMutableArray new];
            for (Station * station in stationsWithNoBikes) {
                [stationsStatus addObject:station.title];
            }
            [message appendString:[stationsStatus componentsJoinedByString:@", "]];
            [message appendString:@"."];
        }
        [[UIApplication sharedApplication] presentNotificationWithSummary:message
                                                               forStation:[_referenceFence.stations firstObject]
                                                                withSound:shouldPlaySound];
    }

    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"StationsStatusSummary.lastSummaryDate"];
}

+ (BOOL) hasNotificationAuthorization
{
    return UIApplication.sharedApplication.currentUserNotificationSettings.types & UIUserNotificationTypeAlert;
}

+ (void) requestNotificationAuthorization
{
    [UIApplication.sharedApplication registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert categories:nil]];
    [NSUserDefaults.standardUserDefaults setBool:YES forKey:@"Authorization.Notifications.Requested"];
}

+ (BOOL) hasRequestedNotificationAuthorization
{
    return [NSUserDefaults.standardUserDefaults boolForKey:@"Authorization.Notifications.Requested"];
}

@end
