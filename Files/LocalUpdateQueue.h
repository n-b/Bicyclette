#import "Locatable.h"
@protocol LocalUpdateQueueDelegate;

@class BicycletteCity;
@class StationsStatusSummary;

//
// Queue
@interface LocalUpdateQueue : NSObject

@property (weak) id<LocalUpdateQueueDelegate> delegate;

- (void) setCity:(BicycletteCity*)city_;
- (void) setVisibleRegion:(MKCoordinateRegion)region_;
- (void) setStarredStations:(NSArray*)stations_;

// One-shot Update Groups
@property (nonatomic) StationsStatusSummary * summary;

@property (nonatomic) BOOL paused;

@end

//
// Updatable “things”: BicycletteCity and Station
@protocol LocalUpdatePoint <NSObject, Locatable>
- (void) updateWithCompletionBlock:(void(^)(NSError* error))completion;
- (NSString*) title;
@end

//
// delegate
@protocol LocalUpdateQueueDelegate <NSObject>
- (void) updateQueueDidComplete:(LocalUpdateQueue *)queue updatedPoints:(NSArray*)points errors:(NSDictionary*)errors;
@end

