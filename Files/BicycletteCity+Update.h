#import "BicycletteCity.h"
#import "DataUpdater.h"
#import "KVCMapping.h"

@interface BicycletteCity (Update)  <DataUpdaterDelegate, LocalUpdatePoint>

- (NSArray *) updateURLStrings;
- (NSString*) detailsURLStringForStation:(Station*)station_;


- (NSDictionary*) KVCMappingDictionary;
- (Class) stationStatusParsingClass; // Default is nil
- (BOOL) canUpdateIndividualStations; // returns yes if stationStatusParsingClass is not nil

- (BOOL) canShowFreeSlots; // returns yes if either status_free or both status_total and status_available are in the KVCMapping

// Data Updates
- (void) updateWithCompletionBlock:(void(^)(NSError* error))completion;

@property (nonatomic, readonly) NSArray * cachedStationsStatus;
- (void) cacheStationsStatus;

// Parsing
- (void) insertStationWithAttributes:(NSDictionary*)stationAttributes;// Call from subclass during parsing
- (void) setStation:(Station*)station attributes:(NSDictionary*)stationAttributes;

@end

/****************************************************************************/
#pragma mark - Methods to be reimplementend by concrete subclasses

@class RegionInfo;
@protocol BicycletteCity

// City Data Updates
@required
- (void) parseData:(NSData*)data;

@end

// Allow me to use method implemented in subclasses
@interface BicycletteCity (BicycletteCity) <BicycletteCity>
@end
