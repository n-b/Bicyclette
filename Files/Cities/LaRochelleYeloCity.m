#import "FlatListCity.h"
#import "NSStringAdditions.h"

@interface LaRochelleYeloCity : FlatListCity <CityWithFlatListOfStations>
@end

@implementation LaRochelleYeloCity

- (NSArray *)updateURLStrings
{
    return @[@"http://yelo.agglo-larochelle.fr/stations?address=&velos=true"];
}

#pragma mark City Data Update

- (NSArray*) stationAttributesArraysFromData:(NSData*)data;
{
    NSString * string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSScanner * scanner = [NSScanner scannerWithString:string];
    NSString * stationsData = nil;
    [scanner scanUpToString:@"createFbMarker(map," intoString:nil];
    [scanner scanUpToString:@"});</script>" intoString:&stationsData];
    if([stationsData length] == 0)
        return nil;

    NSMutableArray * attributesArray = [NSMutableArray new];
    NSScanner * dataScanner = [NSScanner scannerWithString:stationsData];
    while ([dataScanner scanString:@"createFbMarker(map," intoString:nil]) {
        NSString * stationData;
        [dataScanner scanUpToString:@")" intoString:&stationData];
        NSArray * attributes = [stationData componentsSeparatedByString:@","];
        [attributesArray addObject:attributes];
        [dataScanner scanUpToString:@"createFbMarker(map," intoString:nil];
    }
    return attributesArray;
}

- (NSDictionary *)KVCMappingDictionary
{
    return @{
             @0: @"latitude",
             @1: @"longitude",
             @2: @[@"name", @"number"],
             @3: @"status_available",
             @4: @"status_free",
             @5: @"status_total",
        };
}

@end
