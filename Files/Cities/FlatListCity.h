#import "BicycletteCity+Update.h"

@interface FlatListCity : BicycletteCity
- (void) parseData:(NSData*)data;
@end

// To be implemented by subclasses
@protocol CityWithFlatListOfStations <BicycletteCity>
- (NSArray*) stationAttributesArraysFromData:(NSData*)data;
@end

