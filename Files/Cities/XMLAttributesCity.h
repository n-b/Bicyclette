#import "BicycletteCity+Update.h"

@interface XMLAttributesCity : BicycletteCity
- (void) parseData:(NSData*)data;
- (NSString*) stationElementName; // override if necessary
@end

