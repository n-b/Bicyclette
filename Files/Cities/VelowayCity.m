#import "FlatJSONListCity.h"
#import "NSStringAdditions.h"

@interface VelowayCity : FlatJSONListCity
@end

@implementation VelowayCity

#pragma mark Annotations

- (NSString*) titleForStation:(Station *)station {
    NSString * title;
    if([station.address length])
        title = station.address;
    else
        title = station.name;
    title = [title stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    title = [title stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    title = [title capitalizedStringWithCurrentLocale];
    return title;
}

- (NSDictionary *)KVCMappingDictionary
{
    return @{@"ac": @"status_total",
             @"ap": @"status_free",
             @"id": @"number",
             @"wcom": @"address",
             @"lat": @"latitude",
             @"lng": @"longitude",
             @"name": @"name",
             @"ab": @"status_available"
             };
}

- (NSString *)keyPathToStationsLists
{
    return @"stand";
}

@end
