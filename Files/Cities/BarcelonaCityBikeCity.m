#import "XMLSubnodesCity.h"
#import "NSValueTransformer+TransformerKit.h"

@interface BarcelonaCityBikeCity : XMLSubnodesCity
@end

@implementation BarcelonaCityBikeCity

+ (void) initialize
{
    [NSValueTransformer registerValueTransformerWithName:@"BarcelonaStatus" transformedValueClass:[NSNumber class]
                      returningTransformedValueWithBlock:^NSNumber*(NSString* value) {
                          return @([value isKindOfClass:[NSString class]] && [value isEqualToString:@"OPN"]);
                      }];
}

- (NSArray *)updateURLStrings
{
    return @[@"http://wservice.viabicing.cat/getstations.php?v=1"];
}

- (NSString *)stationElementName
{
    return @"station";
}

- (NSString *)titleForStation:(Station *)station
{
    // TODO: station.name contains HTML entities. Unescape it.
    // I would want to do it via NSAttributedString HTML parsing, but as it spins the runloop, it leads to very beautiful, and violent, crashes.
    return [NSString stringWithFormat:@"%@ %@",station.address, station.name];
}

- (NSDictionary *)KVCMappingDictionary
{
    return @{@"id": @"number",
             @"lat": @"latitude",
             @"long": @"longitude",
             @"street": @"name",
             @"streetNumber": @"address",
             @"status": @"BarcelonaStatus:open",
             @"bikes": @"status_available",
             @"slots": @"status_free",
             };
}

@end
