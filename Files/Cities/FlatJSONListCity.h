#import "FlatListCity.h"

// JSON
@interface FlatJSONListCity : FlatListCity
- (NSArray*) stationAttributesArraysFromData:(NSData*)data; // basic JSON deserialize
- (NSString*) keyPathToStationsLists; // override if necessary
@end

