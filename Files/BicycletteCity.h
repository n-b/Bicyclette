#import "CoreDataModel.h"
#import "DataUpdater.h"
#import "LocalUpdateQueue.h"
#import "Station.h"
#import "KVCMapping.h"

void BicycletteCitySetStoresDirectory(NSString* directory);
void BicycletteCitySetSaveStationsWithNoIndividualStatonUpdates(BOOL save);

/****************************************************************************/
#pragma mark - Semi-Abstract superclass.

@interface BicycletteCity : CoreDataModel <Locatable, MKAnnotation, MKOverlay>
{
    void(^_updateCompletionBlock)(NSError*) ;
    NSManagedObjectContext * _parsing_context;
    KVCEntitiesCache * _parsing_cache;
    NSString * _parsing_urlString;
}

// initialization
+ (NSArray*) allCities;
+ (instancetype) cityWithServiceInfo:(NSDictionary*)serviceInfo;

// General properties
- (NSDictionary *) serviceInfo;
- (NSString *) cityName;
- (NSString *) serviceName;
- (NSDictionary*) patches;
- (NSDictionary*) prefs;
- (id) prefForKey:(NSString*)key; // Fallback to NSUserDefaults
- (CLCircularRegion *) knownRegion;
- (CLCircularRegion *) validRegion;
- (CLCircularRegion *) regionContainingData;
- (MKCoordinateRegion) mkRegionContainingData;
- (CLLocation *) location; // Locatable
- (CLLocationDistance) radius;  // Locatable
- (CLLocationCoordinate2D) coordinate; // MKAnnotation

- (NSDictionary*) accountInfo; // credentials info (login/password, apikey) for the city webservice.

// Annotations - override if necessary
- (NSString*) title;
- (NSString*) titleForStation:(Station*)station;
- (NSString *) subtitleForStation:(Station *)station;

// Fetch requests
- (Station*) stationWithNumber:(NSString*)number;
- (NSArray*) stationsWithinRegion:(MKCoordinateRegion)region;
- (NSArray*) starredStations;
- (NSUInteger) numberOfStations;

@property DataUpdater * updater;

@end

/****************************************************************************/
#pragma mark - Update Notifications
extern const struct BicycletteCityNotifications {
	__unsafe_unretained NSString * updateBegan;
	__unsafe_unretained NSString * updateGotNewData;
	__unsafe_unretained NSString * updateSucceeded;
	__unsafe_unretained NSString * updateFailed;
    struct
    {
        __unsafe_unretained NSString * dataChanged;
        __unsafe_unretained NSString * saveErrors;
        __unsafe_unretained NSString * failureError;
    } keys;
} BicycletteCityNotifications;

// Obtain the City from any Station, Region, or Radar.
@interface NSManagedObject (BicycletteCity)
- (BicycletteCity *) city;
@end

