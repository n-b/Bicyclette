@import UIKit;
#import "Station.h"
#import "CitiesController.h"
#import "BicycletteCity.h"
#import "Style.h"
#import "MapVC.h"
#import "ENGoogleMeasurementProtocol.h"

@interface BicycletteApplicationDelegate : UIResponder <UIApplicationDelegate>
@property (nonatomic) UIWindow *window;
@end

@implementation BicycletteApplicationDelegate
{
    CitiesController * _citiesController;
}
#pragma mark Application lifecycle

- (id)init
{
    self = [super init];
    if (self) {
        // Load Factory Defaults
        [[NSUserDefaults standardUserDefaults] registerDefaults:
         [NSDictionary dictionaryWithContentsOfFile:
          [[NSBundle mainBundle] pathForResource:@"FactoryDefaults" ofType:@"plist"]]];
        
        [self setupAnalytics];
    }
    return self;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Clear all notifications.
    [[UIApplication sharedApplication] cancelAllLocalNotifications];

    // Backend
    _citiesController = [CitiesController new];

    // Setup UI and VCs
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.tintColor = kBicycletteBlue;

    MapVC * mapVC = [MapVC mapVCWithController:_citiesController];
    _citiesController.delegate = mapVC;
    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:mapVC];

    [self.window makeKeyAndVisible];
	return YES;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Clear all notifications.
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    if(application.applicationState==UIApplicationStateInactive && [notification.userInfo[@"type"] isEqualToString:@"stationsummary"]) {
        [_citiesController handleLocalNotificaion:notification];
    }
}

#pragma mark - Google Analytics

- (void) setupAnalytics
{
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"DisableGAI"]) {
        ENGAManager *gaMgr = [ENGAManager sharedManager];
        gaMgr.userAgent = [NSString stringWithFormat:@"Bicyclette %@",[NSBundle mainBundle].infoDictionary[(NSString*)kCFBundleVersionKey]];
        NSString *clientID = [gaMgr clientToken];
        NSString * trackingID = [[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"_GoogleAnalytics" ofType:@"plist"]] objectForKey:@"TrackingID"];
        NSString * version = [NSBundle mainBundle].infoDictionary[(id)kCFBundleVersionKey];
        [gaMgr registerDefaults:@{
                                  @(kVersionKey): @"1",
                                  @(kTrackingIDKey): trackingID,
                                  @(kClientIDKey): clientID,
                                  @(kAppNameKey): @"Bicyclette",
                                  @(kAppVersionKey): version
                                  }];
    }
}

@end


int main(int argc, char *argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BicycletteApplicationDelegate class]));
    }
}
