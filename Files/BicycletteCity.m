#import "BicycletteCity.h"
#import "BicycletteCity+Update.h"
#import "LocalUpdateQueue.h"
#import "MKUtilities.h"

@interface BicycletteCity ()
@property NSDictionary* serviceInfo;
@property (nonatomic, readwrite) CLCircularRegion * regionContainingData;
@property (nonatomic, readwrite) NSString* title;
@property (nonatomic, readwrite) CLCircularRegion* knownRegion;
@property (nonatomic, readwrite) CLCircularRegion* validRegion;
@end

#pragma mark -

void BicycletteCitySetStoresDirectory(NSString* directory)
{
    [[NSUserDefaults standardUserDefaults] setObject:directory forKey:@"BicycletteStoresDirectory"];
}

static NSString* BicycletteCityStoresDirectory(void)
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:@"BicycletteStoresDirectory"];
}

void BicycletteCitySetSaveStationsWithNoIndividualStatonUpdates(BOOL save)
{
    [[NSUserDefaults standardUserDefaults] setBool:save forKey:@"BicycletteSaveStationsWithNoIndividualStatonUpdates"];
}

static BOOL BicycletteCitySaveStationsWithNoIndividualStatonUpdates(void)
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"BicycletteSaveStationsWithNoIndividualStatonUpdates"];
}

@implementation BicycletteCity
{
    MKCoordinateRegion _mkRegionContainingData;
    BOOL _mkRegionContainingDataCached;
}
+ (NSString*) storePathForServiceInfo:(NSDictionary*)serviceInfo_
{
    NSString * storeName = [NSString stringWithFormat:@"%@_%@.coredata",serviceInfo_[@"city_name"], serviceInfo_[@"service_name"]];
    return [BicycletteCityStoresDirectory() stringByAppendingPathComponent:storeName];
}

+ (NSArray*) allCities
{
    NSData * data = [NSData dataWithContentsOfFile:[[NSBundle bundleForClass:[self class]] pathForResource:@"BicycletteCities" ofType:@"json"]];
    NSError * error;
    NSArray * serviceInfoArrays = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    NSAssert(serviceInfoArrays!=nil, @"BicycletteCities JSON error: %@", error);
    NSMutableArray * cities = [NSMutableArray new];
    for (NSDictionary * serviceInfo in serviceInfoArrays) {
        BicycletteCity * city = [self cityWithServiceInfo:serviceInfo];
        if(city)
            [cities addObject:city];
    }
    return cities;
}

+ (instancetype) cityWithServiceInfo:(NSDictionary*)serviceInfo_
{
#if DEBUG
    if(serviceInfo_[@"__comment"]!=nil)
        return nil;
#endif
    Class cityClass = NSClassFromString(serviceInfo_[@"city_class"]);
    NSAssert([cityClass isSubclassOfClass:self], nil);
    return [[cityClass alloc] initWithServiceInfo:serviceInfo_];
}

- (id) initWithServiceInfo:(NSDictionary*)serviceInfo_
{
    NSString * storePath;
    if(![self canUpdateIndividualStations] && !BicycletteCitySaveStationsWithNoIndividualStatonUpdates()) //in DataGrabber
    {
        storePath = nil;
    } else {
        storePath = [[self class] storePathForServiceInfo:serviceInfo_];
    }
    self = [super initWithModelName:@"BicycletteCity" storePath:storePath];
    if(self!=nil)
    {
        self.serviceInfo = serviceInfo_;
    }
    return self;
}

- (BOOL) shouldCopyEmbeddedStore
{
    BOOL shouldCopy = [super shouldCopyEmbeddedStore];
    if(!shouldCopy)
    {
        // Copy if the embedded store is more recent than the one in Documents
        NSDate * storeDate = [[[NSFileManager defaultManager] attributesOfItemAtPath:self.storePath error:NULL] fileModificationDate];
        NSDate * embeddedDate = [[[NSFileManager defaultManager] attributesOfItemAtPath:self.embeddedStorePath error:NULL] fileModificationDate];
        shouldCopy = shouldCopy || [embeddedDate compare:storeDate] == NSOrderedDescending;
    }
    return shouldCopy;
}

- (NSDictionary *)storeOptions
{
    if(!BicycletteCitySaveStationsWithNoIndividualStatonUpdates()){  //in DataGrabber
        return @{ NSSQLitePragmasOption : @{@"journal_mode" : @"DELETE"} };
    } else {
        return nil;
    }
}

#pragma mark General properties

- (NSString *) cityName { return _serviceInfo[@"city_name"]; }
- (NSString *) serviceName { return _serviceInfo[@"service_name"]; }
- (NSDictionary*) patches { return self.serviceInfo[@"patches"]; }

- (NSDictionary*) prefs { return self.serviceInfo[@"prefs"]; }
- (id) prefForKey:(NSString*)key
{
    id res = [self prefs][key];
    if(res)
        return res;
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

- (CLCircularRegion *) knownRegion
{
    if(nil==_knownRegion) {
        _knownRegion = [[CLCircularRegion alloc] initWithCenter:CLLocationCoordinate2DMake([self.serviceInfo[@"latitude"] doubleValue],
                                                                                           [self.serviceInfo[@"longitude"] doubleValue])
                                                         radius:[self.serviceInfo[@"radius"] doubleValue] identifier:[self title]];
    }
    return _knownRegion;
}

- (CLCircularRegion*) validRegion
{
    if(nil==_validRegion) {
        _validRegion = [[CLCircularRegion alloc] initWithCenter:self.knownRegion.center
                                                         radius:self.knownRegion.radius * 1.5
                                                     identifier:[self.knownRegion.identifier stringByAppendingString:@"-valid"]];
    }
    return _validRegion;
}

- (CLCircularRegion*) regionContainingData
{
    if(nil==_regionContainingData)
    {
        if( ![self isStoreLoaded] )
            return [self knownRegion];
        
        NSFetchRequest * stationsRequest = [[NSFetchRequest alloc] initWithEntityName:[Station entityName]];
		NSError * requestError = nil;
		NSArray * stations = [self.currentContext executeFetchRequest:stationsRequest error:&requestError];
        if([stations count]==0)
            return [self knownRegion];
        
        CLLocationDegrees maxLatitude = [[stations valueForKeyPath:@"@max.latitude"] doubleValue];
        CLLocationDegrees minLatitude = [[stations valueForKeyPath:@"@min.latitude"] doubleValue];
        CLLocationDegrees maxLongitude = [[stations valueForKeyPath:@"@max.longitude"] doubleValue];
        CLLocationDegrees minLongitude = [[stations valueForKeyPath:@"@min.longitude"] doubleValue];
        CLLocation * dataCenter = [[CLLocation alloc] initWithLatitude:(minLatitude+maxLatitude)/2.0
                                                             longitude:(minLongitude+maxLongitude)/2.0];
        
        CLLocationDistance distanceMax = 0;
        for (Station * station in stations)
            distanceMax = MAX(distanceMax, [station.location distanceFromLocation:dataCenter]);
        
        self.regionContainingData = [[CLCircularRegion alloc] initWithCenter:dataCenter.coordinate
                                                                      radius:distanceMax
                                                                  identifier:[self title]];
    }
    return _regionContainingData;
}

- (MKCoordinateRegion) mkRegionContainingData
{
    if(_mkRegionContainingDataCached) {
        return _mkRegionContainingData;
    }
    
    if(self.serviceInfo[@"mkCoordinateRegionLatitude"] && ![[NSUserDefaults standardUserDefaults] boolForKey:@"DataGrabberComputeCoordinateRegion"]) {
        _mkRegionContainingData.center.latitude = [self.serviceInfo[@"mkCoordinateRegionLatitude"] doubleValue];
        _mkRegionContainingData.center.longitude = [self.serviceInfo[@"mkCoordinateRegionLongitude"] doubleValue];
        _mkRegionContainingData.span.latitudeDelta = [self.serviceInfo[@"mkCoordinateRegionLatitudeDelta"] doubleValue];
        _mkRegionContainingData.span.longitudeDelta = [self.serviceInfo[@"mkCoordinateRegionLongitudeDelta"] doubleValue];
        _mkRegionContainingDataCached = YES;
        return _mkRegionContainingData;
    }
    
    // returns a "rectangle", which is more suited to zoom on the map than a CLCircularRegion:
    // It'll actually zoom differently in landscape or portrait, esp. if the city in very rectangular.
    //
    // We need real data, which means we have to load the city. This means we should avoid doing this early when the app launches.
    NSFetchRequest * stationsRequest = [[NSFetchRequest alloc] initWithEntityName:[Station entityName]];
    NSError * requestError = nil;
    NSArray * stations = [self.currentContext executeFetchRequest:stationsRequest error:&requestError];
    if([stations count]==0)
    {
        CLCircularRegion * region = [self knownRegion];
        return MKCoordinateRegionMakeWithDistance(region.center, region.radius*2, region.radius*2);
    }
    
    _mkRegionContainingData = [stations locatable_region];
    _mkRegionContainingDataCached = YES;
    return _mkRegionContainingData;
}

- (CLLocation *) location
{
    return [[CLLocation alloc] initWithLatitude:self.regionContainingData.center.latitude longitude:self.regionContainingData.center.longitude];
}

- (CLLocationDistance) radius
{
    return self.regionContainingData.radius;
}

- (CLLocationCoordinate2D) coordinate
{
    return self.regionContainingData.center;
}

- (MKMapRect)boundingMapRect
{
    return BICMKMapRectForCoordinateRegion([self mkRegionContainingData]);
}


- (NSDictionary*) accountInfo
{
    NSDictionary * accounts = [NSJSONSerialization JSONObjectWithData:
                               [NSData dataWithContentsOfFile:
                                [[NSBundle bundleForClass:[self class]] pathForResource:@"_Accounts" ofType:@"json"]]
                                    options:0 error:NULL];
    NSDictionary * result = accounts[[NSString stringWithFormat:@"%@_%@",self.serviceInfo[@"city_name"], self.serviceInfo[@"service_name"]]];
    if(result==nil)
        result = accounts[self.serviceInfo[@"account_class"]];
    if(result==nil)
        result = accounts[self.serviceInfo[@"city_name"]];
    if(result==nil)
        result = accounts[self.serviceInfo[@"service_name"]];
    if(result==nil)
        result = accounts[self.serviceInfo[@"city_class"]];
    return result;
}

#pragma mark Fetch requests

- (Station*) stationWithNumber:(NSString*)number
{
    return [Station kvc_fetchObjectInContext:self.currentContext withValue:number forKey:Station.attributes.number options:nil];
}

- (NSArray*) stationsWithinRegion:(MKCoordinateRegion)region
{
    CLLocationDegrees minLatitude = region.center.latitude - region.span.latitudeDelta/2;
    CLLocationDegrees maxLatitude = region.center.latitude + region.span.latitudeDelta/2;
    CLLocationDegrees minLongitude = region.center.longitude - region.span.longitudeDelta/2;
    CLLocationDegrees maxLongitude = region.center.longitude + region.span.longitudeDelta/2;

    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:Station.entityName];
    request.predicate = [NSPredicate predicateWithFormat:
                         @"%K > %f && %K < %f && %K > %f && %K < %f",
                         Station.attributes.latitude, minLatitude, Station.attributes.latitude, maxLatitude,
                         Station.attributes.longitude, minLongitude, Station.attributes.longitude, maxLongitude
                         ];
    
    NSError *error = nil;
    NSArray *stations = [self.currentContext executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"%@ error: %@", NSStringFromSelector(_cmd), error);
    }
    return stations;

}

- (NSArray*) starredStations
{
    NSFetchRequest * request = [NSFetchRequest fetchRequestWithEntityName:Station.entityName];
    request.predicate = [NSPredicate predicateWithFormat:@"%K == YES", Station.attributes.starred];
    
    NSError *error = nil;
    NSArray *result = [self.currentContext executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"%@ error: %@", NSStringFromSelector(_cmd), error);
    }
    return result;
}


- (NSUInteger) numberOfStations
{
    return [self.currentContext countForFetchRequest:[[NSFetchRequest alloc] initWithEntityName:[Station entityName]] error:NULL];
}


#pragma mark Annotations

- (NSString *) title {
    if(nil==_title) {
        _title = [NSString stringWithFormat:@"%@ %@",[self cityName],[self serviceName]];
    }
    return _title;
}
- (NSString *) titleForStation:(Station *)station { return station.name; }

- (NSString *) subtitleForStation:(Station *)station {
    if(station.open==NO) {
        return NSLocalizedString(@"station.status.closed", nil);
    } else if(station.bonus==YES) {
        return NSLocalizedString(@"station.status.bonus", nil);
    } else {
        return nil;
    }
}

@end

/****************************************************************************/
#pragma mark -

@implementation NSManagedObject (BicycletteCity)
- (BicycletteCity *) city
{
    BicycletteCity * city = (BicycletteCity *)[self.managedObjectContext coreDataModel];
    return city;
}
@end

/****************************************************************************/
#pragma mark - Update Notifications

const struct BicycletteCityNotifications BicycletteCityNotifications = {
    .updateBegan = @"BicycletteCityNotifications.updateBegan",
    .updateGotNewData = @"BicycletteCityNotifications.updateGotNewData",
    .updateSucceeded = @"BicycletteCityNotifications.updateSucceded",
    .updateFailed = @"BicycletteCityNotifications.updateFailed",
    .keys = {
        .dataChanged = @"BicycletteCityNotifications.keys.dataChanged",
        .failureError = @"BicycletteCityNotifications.keys.failureError",
        .saveErrors = @"BicycletteCityNotifications.keys.saveErrors",
    }
};
