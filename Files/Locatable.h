@import Foundation;
@import CoreLocation;
@import MapKit;

//
// Locatable
@protocol Locatable <NSObject>
- (CLLocation *) location;
@optional
- (CLLocationDistance) radius;
@end

//
// Collections
@interface NSArray (Locatable)
- (instancetype) sortedArrayByDistanceFromCoordinate:(CLLocationCoordinate2D)coordinate;
// Not strictly the nearest : the locatables' radius is used to weigh in favor of the larger objects.
- (id<Locatable>) nearestLocatableFrom:(CLLocation*)location;
- (MKCoordinateRegion) locatable_region;
@end

