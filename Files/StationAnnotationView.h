#import "MapVC.h"

@interface StationAnnotationView : MKAnnotationView

@property (nonatomic) StationAnnotationMode mode;
@property (nonatomic) BOOL onlyDisplayFavorites;
@end
