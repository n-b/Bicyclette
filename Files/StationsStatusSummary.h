@class Geofence;
@import Foundation;

@interface StationsStatusSummary : NSObject
- (id) initWithFences:(NSArray*)fences_ relativeToFence:(Geofence*)fence_ entered:(BOOL)entered_;
- (void) cancel;
- (void) presentSummary;

- (NSOrderedSet*) stations; // The stations in the fences

+ (BOOL) hasNotificationAuthorization;
+ (void) requestNotificationAuthorization;
+ (BOOL) hasRequestedNotificationAuthorization;
@end
