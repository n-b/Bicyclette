#import "BicycletteCity.h"

@interface BicycletteCity (ServiceDescription)

#pragma mark Service Complete Description
- (NSMutableDictionary *) fullServiceInfo;

@end
