#import "Station+Update.h"

#import <objc/runtime.h>

#import "BicycletteCity+Update.h"
#import "DataUpdater.h"
#import "_StationParse.h"

#pragma mark -

@interface Station (UpdatePrivate)<DataUpdaterDelegate>
@end

@implementation Station (Update)
- (DataUpdater*) updater { return objc_getAssociatedObject(self, @selector(updater)); }
- (void) setUpdater:(DataUpdater*)updater_ { objc_setAssociatedObject(self, @selector(updater), updater_, OBJC_ASSOCIATION_RETAIN); }

- (void(^)(NSError*)) completionBlock { return objc_getAssociatedObject(self, @selector(completionBlock)); }
- (void) setCompletionBlock:(void(^)(NSError*))completionBlock_ { objc_setAssociatedObject(self, @selector(completionBlock), completionBlock_, OBJC_ASSOCIATION_COPY); }

- (void) becomeStale
{
    [[NSNotificationCenter defaultCenter] postNotificationName:StationNotifications.didBecomStale object:self];
}


#pragma mark -

- (void) updateWithCompletionBlock:(void (^)(NSError *))completion_
{
    NSAssert([self.city canUpdateIndividualStations],nil);
    
    if(self.updater)
    {
        if(completion_)
            completion_(nil);
        return;
    }

    if([[NSDate date] timeIntervalSinceDate:self.status_date] < [[NSUserDefaults standardUserDefaults] doubleForKey:@"DataUpdaterDelayBetweenQueues"])
    {
        if(completion_)
            completion_(nil);
        return;
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(becomeStale) object:nil];
    self.completionBlock = completion_;
    self.updater = [[DataUpdater alloc] initWithURLStrings:@[[self.city detailsURLStringForStation:self]] delegate:self];
}

@end

@implementation Station (UpdatePrivate)

- (void) updater:(DataUpdater *)updater didFailWithError:(NSError *)error
{
    self.updater = nil;
    if (self.completionBlock)
        self.completionBlock(error);
    self.completionBlock = nil;
}

- (void) updaterDidFinishWithNoNewData:(DataUpdater *)updater
{
    self.updater = nil;
    if (self.completionBlock)
        self.completionBlock(nil);
    self.completionBlock = nil;
}

- (void) updater:(DataUpdater *)updater finishedWithNewDataChunks:(NSDictionary *)datas
{
    [self.city performUpdates:^(NSManagedObjectContext *updateContext) {
        Station * station = (Station*)[updateContext objectWithID:[self objectID]];
        
        Class parsingClass = [station.city stationStatusParsingClass];
        NSAssert(parsingClass, nil);
        for (NSData * data in [datas allValues]) {
            NSDictionary * attributes = [parsingClass stationAttributesWithData:data];
            [self.city setStation:station attributes:attributes];
        }
    } saveCompletion:^(NSNotification *contextDidSaveNotification) {
        __unused NSManagedObject* updatedObject = [contextDidSaveNotification.userInfo[NSUpdatedObjectsKey] anyObject];
        NSAssert(updatedObject==nil || [updatedObject.objectID isEqual:[self objectID]], nil);
        self.updater = nil;
        if(self.completionBlock)
            self.completionBlock(nil);
        self.completionBlock = nil;
    }];
    
    [self performSelector:@selector(becomeStale) withObject:nil afterDelay:[[NSUserDefaults standardUserDefaults] doubleForKey:@"StationStatusStalenessInterval"]];
}

@end

const struct StationNotifications StationNotifications = {
    .didBecomStale = @"StationNotifications.didBecomStale"
};

@implementation NSDate (BICStaleness)

- (BOOL) bic_isFresh
{
    NSTimeInterval stalenessInterval = [[NSUserDefaults standardUserDefaults] doubleForKey:@"StationStatusStalenessInterval"];
    return [[NSDate date] timeIntervalSinceDate:self] < stalenessInterval;
}

@end
