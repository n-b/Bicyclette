@import CoreData;

#import "LocalUpdateQueue.h"
#import "CollectionsAdditions.h"

#import "StationsStatusSummary.h"
#import "GeofencesMonitor.h"
#import "BicycletteCity.h"
#import "BicycletteCity+Update.h"


@implementation LocalUpdateQueue
{
    BicycletteCity * _city;
    MKCoordinateRegion _visibleRegion;
    NSArray * _starredStations;
 
    NSArray * _pointsInQueue;
    
    BOOL _running;
    UIBackgroundTaskIdentifier _bgTaskIdentifier;
    NSMutableArray * _pointsUpdated;
    NSMutableDictionary * _errors;
}

- (void) setMonitoringPaused:(BOOL)paused_
{
    if(_paused!=paused_) {
        _paused = paused_;
        [self buildUpdateQueue];
        [self startQueue];
    }
}

- (void) setStarredStations:(NSArray *)stations_
{
    if(_starredStations!=stations_) {
        _starredStations = [stations_ copy];
        [self buildUpdateQueue];
        if ([_city canUpdateIndividualStations]) {
            [self startQueue];
        }
    }
}

- (void) setCity:(BicycletteCity*)city_
{
    _city = city_;
    _starredStations = _city.starredStations;
    [self buildUpdateQueue];
    [self startQueue];
}

- (void) setVisibleRegion:(MKCoordinateRegion)region_
{
    _visibleRegion = region_;
    [self buildUpdateQueue];
    if([_city canUpdateIndividualStations]) {
        [self startQueue];
    }
}

- (void) setSummary:(StationsStatusSummary *)summary_
{
    if(_summary!=summary_) {
        _summary = summary_;
        [self buildUpdateQueue];
        [self startQueue];
    }
}

/****************************************************************************/
#pragma mark Data

- (void) buildAndStartQueue
{
    [self buildUpdateQueue];
    [self startQueue];
}

- (void) buildUpdateQueue
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(buildAndStartQueue) object:nil];

    if(![_city canUpdateIndividualStations]) {
        if((_paused && self.summary==nil) || _city==nil) {
            _pointsInQueue = @[];
        } else {
            _pointsInQueue = @[_city];
        }
    } else {
        NSMutableOrderedSet * stations = [NSMutableOrderedSet new];
        if (self.summary) {
            [stations unionOrderedSet:self.summary.stations];
        }
        if (!_paused) {
            [stations addObjectsFromArray:[_starredStations sortedArrayByDistanceFromCoordinate:_visibleRegion.center]];
            [stations addObjectsFromArray:[[_city stationsWithinRegion:_visibleRegion] sortedArrayByDistanceFromCoordinate:_visibleRegion.center]];
        }
        
        _pointsInQueue = [stations array];
    }
}

/****************************************************************************/
#pragma mark Update Loop

- (void) startQueue
{
    if(!_running) {
        _running = YES;
        _pointsUpdated = [NSMutableArray new];
        _errors = [NSMutableDictionary new];
        _bgTaskIdentifier = [UIApplication.sharedApplication beginBackgroundTaskWithExpirationHandler:^{ [self finishQueue]; }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self updateNext];
        });
    }
}

- (void) updateNext
{
    // Find first point we haven't updated in that loop
    id<LocalUpdatePoint> nextPoint = [[_pointsInQueue arrayByRemovingObjectsInArray:_pointsUpdated] firstObject];

    if(nextPoint) {
        [_pointsUpdated addObject:nextPoint];
        UIApplication.sharedApplication.networkActivityIndicatorVisible = YES;
        [nextPoint updateWithCompletionBlock:^(NSError* error){
            if(error) {
                _errors[nextPoint.title] = error;
            }
            // Loop
            [self updateNext];
        }];
    }
    else
    {
        // We've done all the stations in the list !
        UIApplication.sharedApplication.networkActivityIndicatorVisible = NO;
        [UIApplication.sharedApplication endBackgroundTask:_bgTaskIdentifier];
        [self finishQueue];
        
        // clear the oneshot groups and restart after a delay
        self.summary = nil;
        double delay = [[NSUserDefaults standardUserDefaults] doubleForKey:@"DataUpdaterDelayBetweenQueues"];
        [self performSelector:@selector(buildAndStartQueue) withObject:nil afterDelay:delay];
    }
}

- (void) finishQueue
{
    _bgTaskIdentifier = UIBackgroundTaskInvalid;
    _running = NO;
    [_delegate updateQueueDidComplete:self updatedPoints:[_pointsUpdated copy] errors:[_errors copy]];
}

@end

