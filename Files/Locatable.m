#import "Locatable.h"

/****************************************************************************/
#pragma mark Collections

// Add latitude/longitude properties to all <Locatable> objects
// (Pseudo duck-typing for locatable_region)
@interface NSObject (Locatable) <Locatable>
@end
@implementation NSObject (Locatable_Internal)
- (CLLocationDegrees) locatable_latitude
{
    return self.location.coordinate.latitude;
}
- (CLLocationDegrees) locatable_longitude
{
    return self.location.coordinate.longitude;
}
@end

@implementation NSArray (Locatable)

- (instancetype) sortedArrayByDistanceFromCoordinate:(CLLocationCoordinate2D)coordinate
{
    CLLocation * location = [[CLLocation alloc] initWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    return [self sortedArrayUsingComparator:
            ^NSComparisonResult(id<Locatable> l1, id<Locatable> l2) {
                CLLocationDistance d1 = [location distanceFromLocation:[l1 location]];
                CLLocationDistance d2 = [location distanceFromLocation:[l2 location]];
                return d1<d2 ? NSOrderedAscending : d1>d2 ? NSOrderedDescending : NSOrderedSame;
            }];
}

- (id<Locatable>) nearestLocatableFrom:(CLLocation*)location
{
    __block id<Locatable> result = nil;
    [self enumerateObjectsUsingBlock:^(id<Locatable> obj, NSUInteger idx, BOOL *stop) {
        if(!result)
        {
            result = obj;
            return;
        }
        
        CLLocationDistance d1 = [location distanceFromLocation:[result location]];
        CLLocationDistance d2 = [location distanceFromLocation:[obj location]];
        
        // The radius is used to weigh the distance, in order to favor larger objects.
        //
        // The idea is that a big object should be considered "nearer" than a very small object.
        // If location is 1500m from from a big object (r=1000m), and 500m from a small object (r=10m),
        // The "distances" are : 1.5 to the big object, and 50 to the small object.
        //
        // Basically, I want Vélib to be more important than Cristolib.
        if([result respondsToSelector:@selector(radius)] && [obj respondsToSelector:@selector(radius)])
        {
            // Some cities have only one station, i.e. a zero radius.
            CLLocationDistance minRadius = [[NSUserDefaults standardUserDefaults] doubleForKey:@"Cities.MinimumRadius"];
            d1 /= MAX([result radius], minRadius);
            d2 /= MAX([obj radius], minRadius);
        }
        
        if(d2<d1)
            result = obj;
    }];
    
    return result;
}

- (MKCoordinateRegion) locatable_region
{
    CLLocationDegrees maxLatitude = [[self valueForKeyPath:@"@max.locatable_latitude"] doubleValue];
    CLLocationDegrees minLatitude = [[self valueForKeyPath:@"@min.locatable_latitude"] doubleValue];
    CLLocationDegrees maxLongitude = [[self valueForKeyPath:@"@max.locatable_longitude"] doubleValue];
    CLLocationDegrees minLongitude = [[self valueForKeyPath:@"@min.locatable_longitude"] doubleValue];
    
    MKCoordinateRegion region;
    region.center.latitude = (minLatitude+maxLatitude)/2.0;
    region.center.longitude = (minLongitude+maxLongitude)/2.0;
    region.span.latitudeDelta = maxLatitude - minLatitude;
    region.span.longitudeDelta = maxLongitude - minLongitude; // incorrect at -/+180
    
    return region;
}

@end
