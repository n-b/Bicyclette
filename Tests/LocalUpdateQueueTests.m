#import "LocalUpdateQueue.h"
#import "SenTestCase+RunLoop.h"

//
// Make NSDictionary conform to the protocols to use as our test objects
@interface NSDictionary (Locatable)  <Locatable, LocalUpdateGroup, LocalUpdatePoint>
@end
@implementation NSDictionary (Locatable)
- (id) location
{
    return self[@"location"];
}
- (NSArray*) pointsToUpdate
{
    return self[@"points"];
}
- (void) updateWithCompletionBlock:(void (^)(NSError* e))completion
{
    void (^updateblock)(id obj) = self[@"update"];
    if(updateblock)
        updateblock(self);
    completion(nil);
}
@end

//
// Tests
@interface NSArray_LocatableTests : XCTestCase
@end

@implementation NSArray_LocatableTests

- (void) testSortedArray
{
    NSArray * testdata = @[
                           @{@"id": @1, @"location": [[CLLocation alloc] initWithLatitude:45 longitude:2]},
                           @{@"id": @2, @"location": [[CLLocation alloc] initWithLatitude:46 longitude:3]},
                           @{@"id": @3, @"location": [[CLLocation alloc] initWithLatitude:47.2 longitude:2.2]},
                           @{@"id": @4, @"location": [[CLLocation alloc] initWithLatitude:47.2 longitude:2]},
                           @{@"id": @5, @"location": [[CLLocation alloc] initWithLatitude:47 longitude:2]},
                           ];
    
    id result = [testdata sortedArrayByDistanceFromLocation:[[CLLocation alloc] initWithLatitude:47 longitude:2]];
    XCTAssertEqualObjects([result valueForKeyPath:@"id"], (@[@5, @4, @3, @2, @1]));
}

@end

/****************************************************************************/
#pragma mark -

@interface LocalUpdateQueueTests : XCTestCase <LocalUpdateQueueDelegate>
@end

@implementation LocalUpdateQueueTests
{
    LocalUpdateQueue * queue;
    id g1, g2;
    id p1, p2;
    __block BOOL delegateCalledFlag;
}

- (void) setUp
{
    [super setUp];
    queue = [LocalUpdateQueue new];
    queue.referenceLocation = [[CLLocation alloc] initWithLatitude:0 longitude:0];
    queue.delayBetweenPointUpdates = .01;
    
    delegateCalledFlag = NO;
}

- (void) testMonitoredGroups
{
    // Prepare a group of local points
    p1 = [@{@"location": [[CLLocation alloc] initWithLatitude:0 longitude:.5]} mutableCopy];
    p2 = [@{@"location": [[CLLocation alloc] initWithLatitude:0 longitude:-.5]} mutableCopy];
    g1 = @{@"location" : [[CLLocation alloc] initWithLatitude:0 longitude:1], @"points" : @[p1, p2]};
    
    __block BOOL completed = NO;
    __block int updateCount = 0;
    p1[@"update"] = ^(id obj){ updateCount ++; completed = updateCount==5; };

    // Add our group
    [queue addMonitoredGroup:g1];
    
    // Check "update" is called repeatedly
    [self waitForCompletion:.1 flag:&completed];
    XCTAssertTrue(completed);
    XCTAssertEqual(updateCount, 5);
}

- (void) testOneshotGroups
{
    // Prepare a group of local points
    p1 = [@{@"location": [[CLLocation alloc] initWithLatitude:0 longitude:.5]} mutableCopy];
    p2 = [@{@"location": [[CLLocation alloc] initWithLatitude:0 longitude:-.5]} mutableCopy];
    g1 = @{@"location" : [[CLLocation alloc] initWithLatitude:0 longitude:1], @"points" : @[p1, p2]};
    
    // Set a test update block to count calls
    __block int updateCount = 0;
    p1[@"update"] = ^(id obj){ updateCount ++; };
    
    // Add our group
    [queue addOneshotGroup:g1];
    
    // Check "update" is called only once
    [self waitForCompletion:.05 flag:NULL];
    
    XCTAssertEqual(updateCount, 1);
}

- (void) testOneshotDelegate
{
    // Prepare a reference points and a group of local points
    queue.delegate = self;
    
    p1 = [@{@"location": [[CLLocation alloc] initWithLatitude:0 longitude:.5]} mutableCopy];
    g1 = @{@"location" : [[CLLocation alloc] initWithLatitude:0 longitude:1], @"points" : @[p1]};
        
    // Add our group
    [queue addOneshotGroup:g1];
        
    // Check "update" is called only once
    [self waitForCompletion:.05 flag:&delegateCalledFlag];
    
    XCTAssertTrue(delegateCalledFlag);
}

- (void) updateQueueDidComplete:(LocalUpdateQueue *)queue updatedPoints:(NSArray*)points errors:(NSArray*)errors {}

- (void) testUpdatedPointsOrder
{
    // Prepare groups of local points    
    p1 = [@{@"location": [[CLLocation alloc] initWithLatitude:0 longitude:0]} mutableCopy];
    g1 = @{@"location" : [[CLLocation alloc] initWithLatitude:0 longitude:0], @"points" : @[p1]};
    
    p2 = [@{@"location": [[CLLocation alloc] initWithLatitude:0 longitude:0]} mutableCopy];
    g2 = @{@"location" : [[CLLocation alloc] initWithLatitude:0 longitude:1], @"points" : @[p2]};
    
    // Add our groups
    [queue addMonitoredGroup:g1];
    [queue addMonitoredGroup:g2];

    NSMutableArray * updates = [NSMutableArray new];
    p1[@"update"] = p2[@"update"] = ^(id obj){ [updates addObject:obj]; };

    [self waitForCompletion:.025 flag:NULL]; // Just give it enough time for 2 updates
    XCTAssertEqualObjects(updates, (@[p1,p2,p1,p2]));
}

@end
