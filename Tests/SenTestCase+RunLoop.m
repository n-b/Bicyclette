#import "SenTestCase+RunLoop.h"

@implementation XCTestCase (RunLoop)

- (BOOL)waitForCompletion:(NSTimeInterval)timeoutSecs flag:(BOOL*)completed {
    NSDate *timeoutDate = [NSDate dateWithTimeIntervalSinceNow:timeoutSecs];
    
    do {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:nil];
        if([timeoutDate timeIntervalSinceNow] < 0.0)
            break;
    } while (completed==NULL || !(*completed));
    
    return completed!=NULL && (*completed);
}

@end
